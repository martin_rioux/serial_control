# Logiciel de control en python avec pyQt5
Réalisé avec python 3.7 et pyQt5.
Le code exemple permet le fonctionnement de 2 moteurs sur un pont en H, ainsi que le contrôle de 2 servo moteurs.

## Installation
### Linux
sudo apt install python3-pip
`pip3 install --upgrade pip`
`pip3 install setuptools PyQt5 pyserial`

### Window
Aller sur le site de python et télécharger la version 3
Installer 
IMPORTANT: Il faut cocher l'option 'ADD PYTHON TO PATH', sinon ça ne marchera pas!
Ouvrir un terminal PowerShell en administrateur et exécuter les commandes
`pip install --upgrade pip`
`pip install PyQt5 pyserial`

## Utilisation
Lancer le logiciel par la commande `python3 remote_control.py`, ou cliquez dessus.

### Mode Serial
- S'assurer des paramètres Device et Baud. Normalement Timeout et Thread Sleep ne devraient pas avoir besoin d'être changés.
- Faire connect

### Mode Socket
- S'assurer des paramètres Address et Port. Normalement Buffer, Timeout et Thread Sleep ne devraient pas avoir besoin d'être changés.
- Faire connect


## Fonctionnement

### Commandes continues
Il s'agit de commandes qui sont envoyées en continus. Généralement important pour ce qui est de la motricité.

Pour rajouter une commande continue, ajouter la touche dans le dictionnaire `continuous_transmission_keys`
```
continuous_transmission_keys = {
    "W":"w",
    "A":"a",
    "S":"s",
	[...]
    "L":"l"
    }
```

Et rajouter la conditions dans la fonction principale (`def specialKeyAction`) tel que:
```
    if e.key() == QtCore.Qt.Key_W:
        continuous_transmission_keys["W"] = "W" if state == "pressed" else "w"
```

Notez qu'il a été décidé qu'une majuscule 

### Commandes instantanées
Il s'agit de commandes qui sont envoyé seulement lorsque le boutton est pressé.

- Pour rajouter des commandes séries, modifier la première fonction du code python. Il est possible de réaliser un action sur une touche pressé et/ou relachée. Par exemple:
```python
if e.key() == QtCore.Qt.Key_W and state == "pressed":
	qt.sendSerialData("PW")
if e.key() == QtCore.Qt.Key_W and state == "released":
	qt.sendSerialData("RW")
```
QtCore.Qt.Key_W spécifie la touche W et state spécifie l'état.
N'importe quel texte peut être saisis dans sendSerialData, il sufit que l'arduino soit capable de l'interpréter!

IMPORTANT: N'employez pas le charactère % comme premier caractère transmit, car c'est le moyen pour l'Arduino de savoir qu'il s'agit d'une transmission multiple