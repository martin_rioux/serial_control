
# By Martin Rioux -- martin.rioux2@gmail.com
# To use as you wish with proper credit!
# Last update 2019-11-03

# Add every key that you want to be transmitted continuously here
continuous_transmission_keys = {
    "W":"w",
    "A":"a",
    "S":"s",
    "D":"d",
    "I":"i",
    "J":"j",
    "K":"k",
    "L":"l"
    }

def specialKeyAction(qt, e, state):
    if e.key() == QtCore.Qt.Key_Escape:
        qt.close()

    # Continuous keys
    if e.key() == QtCore.Qt.Key_W:
        continuous_transmission_keys["W"] = "W" if state == "pressed" else "w"
    elif e.key() == QtCore.Qt.Key_A:
        continuous_transmission_keys["A"] = "A" if state == "pressed" else "a"
    elif e.key() == QtCore.Qt.Key_S:
        continuous_transmission_keys["S"] = "S" if state == "pressed" else "s"
    elif e.key() == QtCore.Qt.Key_D:
        continuous_transmission_keys["D"] = "D" if state == "pressed" else "d"


    elif e.key() == QtCore.Qt.Key_I:
        continuous_transmission_keys["I"] = "I" if state == "pressed" else "i"
    elif e.key() == QtCore.Qt.Key_J:
        continuous_transmission_keys["J"] = "J" if state == "pressed" else "j"
    elif e.key() == QtCore.Qt.Key_K:
        continuous_transmission_keys["K"] = "K" if state == "pressed" else "k"
    elif e.key() == QtCore.Qt.Key_L:
        continuous_transmission_keys["L"] = "L" if state == "pressed" else "l"

    # Keys that are only transmitted when pressed
    if e.key() == QtCore.Qt.Key_1 and state == "pressed":
        qt.sendSerialData("P1")
    if e.key() == QtCore.Qt.Key_2 and state == "pressed":
        qt.sendSerialData("P2")
    if e.key() == QtCore.Qt.Key_7 and state == "pressed":
        qt.sendSerialData("P7")
    if e.key() == QtCore.Qt.Key_8 and state == "pressed":
        qt.sendSerialData("P8")

    if e.key() == QtCore.Qt.Key_B and state == "pressed":
        qt.sendSerialData("PB")
    if e.key() == QtCore.Qt.Key_B and state == "released":
        qt.sendSerialData("RB")


def receivedDataHandler(qt, data):
    data = data.rstrip()

    # if data == "Oh Hai Mark!":
    #     qt.total += (time.time()*1000 - qt.timer)
    #     qt.qte += 1
    #     print(qt.total/qt.qte)l











# REAL STUFF
DEBUG_MODE = False


import sys, threading, time
from PyQt5 import QtGui, QtCore, QtWidgets
from serial import Serial
import socket

# print (serial.tools.list_ports.comports(include_links=False))

class RoboControl(QtWidgets.QWidget):

    def __init__(self):
        super(RoboControl, self).__init__()
        self.timer = 0
        self.total = 0
        self.qte = 0
        self.PRESSED_KEYS = set() # key currently pressed
        self.COMMAND_TO_SEND = []
        self.SERIAL_DATA_SENT = []
        self.SERIAL_DATA_RECEIVED = []
        self.SERIAL_INPUT = ""
        self.CONNECTED = DEBUG_MODE
        self.COM_MODE = "SOCKET"  #SOCKET or SERIAL
        # GENERAL CONFIG
        self.READ_TIMEOUT = 0.0002 # 0.002 for serial, 0.005 socket or more?, finally 0.0002 is good?
        # IF SERIAL MODE
        self.DEVICE = "COM"
        self.BAUD = 115200
        # IF SOCKET MODE
        self.ADDRESS = "192.168.8.1"
        self.RECV_BUFFER = 1024
        self.PORT = 7777
        self.THREAD_SLEEP = 0.0002
        self.CONTINUOUS_TRANSMISSION_SLEEP = 0.02
        self.initUI()
        self.displayThreadData()

        self.running = 1
        self.thread1 = threading.Thread(target=self.serialThread)
        self.thread1.start()

        self.thread2 = threading.Thread(target=self.continuousTransmissionThread)
        self.thread2.start()

    def connectToArduino(self):

        self.total = 0
        self.qte = 0
        if self.CONNECTED:
            self.disconnectArduino()
            return
        if self.COM_MODE == "SERIAL":
            try:
                self.DEVICE = str(self.deviceInput.text())
                self.BAUD = int(self.baudInput.text())
                self.READ_TIMEOUT = float(self.readTimeoutSerialInput.text())
                self.THREAD_SLEEP = float(self.threadSleepSerialInput.text())
                if self.THREAD_SLEEP <= 0:
                    self.THREAD_SLEEP = 0.0000001
                    self.threadSleepSerialInput.setText("0.0000001")
                self.ser = Serial(self.DEVICE, self.BAUD)
                self.ser.timeout = 2
                self.CONNECTED = True
                self.ser.timeout = self.READ_TIMEOUT
                self.connectionStatus.setText("Status: CONNECTED")
                self.connectButtonSerial.setText("Disconnect")
            except:
                self.CONNECTED = False
                self.connectionStatus.setText("Status: CONNECTION FAILED")

        if self.COM_MODE == "SOCKET":
            try:
                self.ADDRESS = self.addressInput.text()
                self.PORT = int(self.portInput.text())
                self.RECV_BUFFER = int(self.comBufferInput.text())
                self.READ_TIMEOUT = float(self.readTimeoutSocketInput.text())
                self.THREAD_SLEEP = float(self.threadSleepSocketInput.text())
                if self.THREAD_SLEEP <= 0:
                    self.THREAD_SLEEP = 0.0000001
                    self.threadSleepSocketInput.setText("0.0000001")
                self.ser = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.ser.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                self.ser.setblocking(0)
                self.ser.settimeout(2)
                self.ser.connect((self.ADDRESS, self.PORT))
                self.ser.settimeout(self.READ_TIMEOUT)
                self.CONNECTED = True
                self.connectionStatus.setText("Status: CONNECTED")
                self.connectButtonSocket.setText("Disconnect")
            except:
                self.CONNECTED = False
                self.connectionStatus.setText("Status: CONNECTION FAILED")

    def initUI(self):
        self.grid = QtWidgets.QGridLayout()
        self.grid.setSpacing(10)

        self.selectCom = QtWidgets.QComboBox()
        self.selectCom.addItem("SERIAL")
        self.selectCom.addItem("SOCKET")
        self.selectCom.currentIndexChanged.connect(self.changeComType)
        self.selectCom.setFixedSize(200, 30)
        self.grid.addWidget(self.selectCom, 0, 0)

        self.connectionStatus = QtWidgets.QLabel('Status: OFFLINE')
        self.connectionStatus.setFont(QtGui.QFont('SansSerif', 14))
        credit = QtWidgets.QLabel("RoboControl \nBy Martin Rioux - martin.rioux2@gmail.com")
        credit.setFont(QtGui.QFont('SansSerif', 8))

        status_and_credit = QtWidgets.QHBoxLayout()
        status_and_credit.addWidget(self.connectionStatus)
        status_and_credit.addWidget(credit)
        self.grid.addLayout(status_and_credit, 0, 1)

        #GENERAL CONFIGS

        self.configs = QtWidgets.QHBoxLayout()
        self.grid.addLayout(self.configs, 1, 0, 1, 2)

        ## SERIAL OUT ##
        serialOutLabel = QtWidgets.QLabel('Serial Out')
        self.serialOutText = QtWidgets.QTextEdit()
        self.serialOutText.setReadOnly(True)

        outBox = QtWidgets.QVBoxLayout()
        outBox.addWidget(serialOutLabel)
        outBox.addWidget(self.serialOutText)
        self.grid.addLayout(outBox, 2, 0)

        ## SERIAL IN ##
        serialInLabel = QtWidgets.QLabel('Serial In')
        self.serialInText = QtWidgets.QTextEdit()
        self.serialInText.setReadOnly(True)

        inBox = QtWidgets.QVBoxLayout()
        inBox.addWidget(serialInLabel)
        inBox.addWidget(self.serialInText)
        self.grid.addLayout(inBox, 2, 1)

        self.setLayout(self.grid)

        self.setGeometry(0,0,1024,768)
        self.center()
        self.setWindowTitle('RoboControl')
        self.show()
        self.changeComType()

    def deleteConfig(self, layout):
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.deleteConfig(item.layout())

    def buildSerialConfigs(self):
        self.deviceLabel = QtWidgets.QLabel('Device')
        self.deviceInput = QtWidgets.QLineEdit(self.DEVICE)
        self.baudLabel = QtWidgets.QLabel('Baud')
        self.baudInput = QtWidgets.QLineEdit(str(self.BAUD))
        self.baudInput.setValidator(QtGui.QIntValidator())
        self.readTimeoutSerialLabel = QtWidgets.QLabel('Timeout (sec) [0.0002]')
        self.readTimeoutSerialInput = QtWidgets.QLineEdit(str(self.READ_TIMEOUT))
        self.readTimeoutSerialInput.setValidator(QtGui.QDoubleValidator())
        self.threadSleepSerialLabel = QtWidgets.QLabel('Thread sleep (sec) [0.0002]')
        self.threadSleepSerialInput = QtWidgets.QLineEdit(str(self.THREAD_SLEEP))
        self.threadSleepSerialInput.setValidator(QtGui.QDoubleValidator())
        self.connectButtonSerialLabel = QtWidgets.QLabel('')
        self.connectButtonSerial = QtWidgets.QPushButton('Connect', self)
        self.connectButtonSerial.clicked.connect(self.connectToArduino)

        device = QtWidgets.QVBoxLayout()
        device.addWidget(self.deviceLabel)
        device.addWidget(self.deviceInput)
        baud = QtWidgets.QVBoxLayout()
        baud.addWidget(self.baudLabel)
        baud.addWidget(self.baudInput)
        readtimeoutSerial = QtWidgets.QVBoxLayout()
        readtimeoutSerial.addWidget(self.readTimeoutSerialLabel)
        readtimeoutSerial.addWidget(self.readTimeoutSerialInput)
        threadSleepSerial = QtWidgets.QVBoxLayout()
        threadSleepSerial.addWidget(self.threadSleepSerialLabel)
        threadSleepSerial.addWidget(self.threadSleepSerialInput)
        connectBSerial = QtWidgets.QVBoxLayout()
        connectBSerial.addWidget(self.connectButtonSerialLabel)
        connectBSerial.addWidget(self.connectButtonSerial)

        self.configs.addLayout(device)
        self.configs.addLayout(baud)
        self.configs.addLayout(readtimeoutSerial)
        self.configs.addLayout(threadSleepSerial)
        self.configs.addLayout(connectBSerial)

    def buildSocketConfigs(self):
        self.addressLabel = QtWidgets.QLabel('Address')
        self.addressInput = QtWidgets.QLineEdit(self.ADDRESS)
        self.portLabel = QtWidgets.QLabel('Port')
        self.portInput = QtWidgets.QLineEdit(str(self.PORT))
        self.portInput.setValidator(QtGui.QIntValidator())
        self.comBufferLabel = QtWidgets.QLabel('Buffer [1024]')
        self.comBufferInput = QtWidgets.QLineEdit(str(self.RECV_BUFFER))
        self.comBufferInput.setValidator(QtGui.QIntValidator())
        self.readTimeoutSocketLabel = QtWidgets.QLabel('Timeout (sec) [0.0002]')
        self.readTimeoutSocketInput = QtWidgets.QLineEdit(str(self.READ_TIMEOUT))
        self.readTimeoutSocketInput.setValidator(QtGui.QDoubleValidator())
        self.threadSleepSocketLabel = QtWidgets.QLabel('Thread sleep (sec) [0.0002]')
        self.threadSleepSocketInput = QtWidgets.QLineEdit(str(self.THREAD_SLEEP))
        self.threadSleepSocketInput.setValidator(QtGui.QDoubleValidator())
        self.connectButtonSocketLabel = QtWidgets.QLabel('')
        self.connectButtonSocket = QtWidgets.QPushButton('Connect', self)
        self.connectButtonSocket.clicked.connect(self.connectToArduino)

        address = QtWidgets.QVBoxLayout()
        address.addWidget(self.addressLabel)
        address.addWidget(self.addressInput)
        port = QtWidgets.QVBoxLayout()
        port.addWidget(self.portLabel)
        port.addWidget(self.portInput)
        comBuffer = QtWidgets.QVBoxLayout()
        comBuffer.addWidget(self.comBufferLabel)
        comBuffer.addWidget(self.comBufferInput)
        readtimeoutSocket = QtWidgets.QVBoxLayout()
        readtimeoutSocket.addWidget(self.readTimeoutSocketLabel)
        readtimeoutSocket.addWidget(self.readTimeoutSocketInput)
        threadSleepSocket = QtWidgets.QVBoxLayout()
        threadSleepSocket.addWidget(self.threadSleepSocketLabel)
        threadSleepSocket.addWidget(self.threadSleepSocketInput)
        connectBSocket = QtWidgets.QVBoxLayout()
        connectBSocket.addWidget(self.connectButtonSocketLabel)
        connectBSocket.addWidget(self.connectButtonSocket)

        self.configs.addLayout(address)
        self.configs.addLayout(port)
        self.configs.addLayout(comBuffer)
        self.configs.addLayout(readtimeoutSocket)
        self.configs.addLayout(threadSleepSocket)
        self.configs.addLayout(connectBSocket)

    def changeComType(self):
        self.disconnectArduino()
        self.COM_MODE = self.selectCom.currentText()
        self.deleteConfig(self.configs)
        if self.COM_MODE == "SERIAL":
            self.displaySerialConfig()
        elif self.COM_MODE == "SOCKET":
            self.displaySocketConfig()

    def displaySerialConfig(self):
        self.buildSerialConfigs()

    def displaySocketConfig(self):
        self.buildSocketConfigs()

    def disconnectArduino(self):
        try:
            self.ser.close()
        except: pass
        if self.CONNECTED:
            if self.COM_MODE == "SOCKET":
                self.connectButtonSocket.setText("Connect")
            if self.COM_MODE == "SERIAL":
                self.connectButtonSerial.setText("Connect")
        self.CONNECTED = False
        self.connectionStatus.setText("Status: OFFLINE")

    def center(self):
        qr = self.frameGeometry()
        cp = QtWidgets.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def onChanged(self, text):
        self.lbl.setText(text)
        self.lbl.adjustSize()

    def keyPressEvent(self, e):
        if e.isAutoRepeat() == True:
            return
        specialKeyAction(self, e, "pressed")
        e.accept()

    def keyReleaseEvent(self, e):
        if e.isAutoRepeat() == True:
            return
        specialKeyAction(self, e, "released")
        e.accept()

    def sendSerialData(self, data):
        if self.CONNECTED:
            self.COMMAND_TO_SEND.append(data + "\n")


    def displayThreadData(self):
        while len(self.SERIAL_DATA_SENT) >= 1:
            data = self.SERIAL_DATA_SENT[0]
            del self.SERIAL_DATA_SENT[0]
            self.serialOutText.moveCursor(QtGui.QTextCursor.End)
            self.serialOutText.insertPlainText(data)

        while len(self.SERIAL_DATA_RECEIVED) >= 1:
            data = self.SERIAL_DATA_RECEIVED[0]
            del self.SERIAL_DATA_RECEIVED[0]
            self.serialInText.moveCursor(QtGui.QTextCursor.End)
            self.serialInText.insertPlainText(data)
            receivedDataHandler(self, data)

        QtCore.QTimer.singleShot(10, self.displayThreadData)

    def closeEvent(self, event):
        reply = QtWidgets.QMessageBox.question(self, 'Warning',
            "Really want to exit?", QtWidgets.QMessageBox.Yes |
            QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No)
        if reply == QtWidgets.QMessageBox.Yes:
            self.running = 0
            event.accept()
        else:
            event.ignore()

    def continuousTransmissionThread(self):
        while self.running:
            if self.CONNECTED:
                transmission = "%"
                for k,v in continuous_transmission_keys.items():
                    transmission += v
                self.sendSerialData(transmission)
            time.sleep(self.CONTINUOUS_TRANSMISSION_SLEEP)

    def serialThread(self):
        while self.running:
            if self.CONNECTED:
                data = ""
                try:
                    if self.COM_MODE == "SERIAL":
                        data += self.ser.readline().decode('utf-8')
                    if self.COM_MODE == "SOCKET":
                        data = self.ser.recv(self.RECV_BUFFER).decode('utf-8')
                except : pass

                if data != "":
                    self.SERIAL_INPUT += data
                    if (self.SERIAL_INPUT.find("\n") != -1):
                        # print(self.SERIAL_INPUT)
                        self.SERIAL_DATA_RECEIVED.append(self.SERIAL_INPUT)
                        self.SERIAL_INPUT = ""

                # print (self.ser.out_waiting)
                if len(self.COMMAND_TO_SEND) >= 1:
                    data = self.COMMAND_TO_SEND[0]
                    del self.COMMAND_TO_SEND[0]
                    self.SERIAL_DATA_SENT.append(data)
                    if DEBUG_MODE == False:
                        try:
                            if self.COM_MODE == "SERIAL":
                                self.ser.write(data.encode('utf-8'))
                            if self.COM_MODE == "SOCKET":
                                self.ser.send(data.encode('utf-8'))
                        except : pass


            # time.sleep(0.0000002) #MAGIC VALUE?
            time.sleep(self.THREAD_SLEEP)

def main():
    app = QtWidgets.QApplication(sys.argv)
    ex = RoboControl()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
