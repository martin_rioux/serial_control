# Logiciel de contrôle
Il s'agit de la version de base (template).
Le mieu est de faire une copie et l'adapter aux besoins

Il y a trois sections importante

## 1 - arduino_serial_controller
Il s'agit du code arduino à employer pour l'utilisation avec le logiciel de contrôle.
Même si le logiciel de contrôle n'est pas employé au final, il peut-être utile pour des tests.

## 2 - logiciel_python
Il s'agit du logiciel de contrôle. Il est capable de contrôler un arduino par port Série ou si employé avec un Raspberry Pi

## 3 - raspberry_pi
Il s'agit d'une image du raspberry pi pouvant être flashé sur une carte SD.

# Utilisation selon les compétition
## CQI
Le code arduino doit être modifier selon les besoins.
Si le contrôle peut-être effectué depuis un ordinateur, employer le logiciel python.
Il y a possibilité d'utiliser une matte de PS2 aussi avec le code.

## CCI
Voir CQI, et bravo pour votre victoire!


# Machine des jeux
Le code arduino doit être modifier selon les besoins.
Si le contrôle peut-être effectué depuis un ordinateur, employer le logiciel python.
Si le contrôle veut être fait à distance à l'aide d'un Raspberry Pi, voir section Raspberry Pi

