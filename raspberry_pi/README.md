# Raspberry Pi
De préférence, être un initié à linux
Présentement, le stream video ne fonctionne que si vous avec un ordinateur avec linux.

## Informations
SSDI du WiFi: 		uqarpi
Mot de passe Wifi: 	martino01
Mot de passe sudo: 	uqar01

## Pour lancer le stream video
Sur votre ordinateur, exécuter la commande 
	`nc -l 2222 | mplayer -fps 200 -demuxer h264es -`
Sur le Raspberry Pi, exécuter la commande
	`/opt/vc/bin/raspivid -t 0 -w 300 -h 300 -hf -fps 20 -o - | nc <VOTRE-IP> 2222`

## Pour Lancer le contrôle à distance
Sur le Raspberry Pi, mettre l'arduino sur le réseau par la commande
	`python tcp_serial_redirect.py /dev/ttyUSB0 115200`
	NOTE: Il se peut que votre arduino aie un autre nom que /dev/ttyUSB0, branchez le et écrivez la commande `dmesg` pour voir son nom. Le baudrate peut également être ajusté selon vos besoins.

Sur votre ordinateur, lancez le logiciel de contrôle remote_control.py
	`python3 ./remote_control.py`


## Installation

Normalement, simplement flasher une carte microSD avec UQARPI.img est suffisant.

Si jamais la configuration doit être refaite à partir d'une installation neuve, voici les changements à réaliser:

Note: C'est horrible a faire et vous risquez de vous battre en chien

- sudo apt-get install hostapd dnsmasq udhcpd python-serial
- Activer le service ssh-server (à l'aide de raspi-config)
- Copier le fichier hostapd.conf dans /etc/hostapd/hostapd.conf
- Apportez les changements suivants:
```bash
-Commande zcat /usr/share/doc/hostapd/examples/hostapd.conf.gz | sudo tee -a /etc/hostapd/hostapd.conf
-Fichier /etc/hostapd/hostapd.conf
	interface=wlan0
	ssid=uqarpi
	hw_mode=g
	wpa=2
	wpa_passphrase=martino01
	wpa_key_mgmt=WPA-PSK WPA-EAP WPA-PSK-SHA256 WPA-EAP-SHA256

-Fichier /etc/network/interfaces
	auto lo
	iface lo inet loopback

	auto wlan0
	iface wlan0 inet static
	hostapd /etc/hostapd/hostapd.conf
	address 192.168.8.1
	netmask 255.255.255.0

-Fichier /etc/dnsmasq.conf
	interface=lo,wlan0
	no-dhcp-interface=lo
	dhcp-range=192.168.8.20,192.168.8.254,255.255.255.0,12h

-Fichier /etc/sysctl.conf
		net.ipv4.ip_forward=1

-Fichier /etc/rc.local (au dessus de exit 0)
	iptables -t nat -A POSTROUTING -s 192.168.8.0/24 ! -d 192.168.8.0/24  -j MASQUERADE

-Certaines cartes ont une switch virtuelle, si c``est le cas faire un commande du type:  rfkill unblock 0

-Fichier /etc/NetworkManager/NetworkManager.conf (SI NetworkManager est présent)
	[main]
	plugins=ifupdown,keyfile,ofono
	dns=dnsmasq

	[ifupdown]
	managed=false

-Copier tcp_serial_redirect.py dans le ~/ du raspberry pi
-Copier feed_serial.sh dans le ~/ du raspberry pi
-Si  ça ne fonctionne pas, vérifiez sur des forums
```