#!/bin/bash

python tcp_serial_redirect.py /dev/ttyUSB0 115200
python tcp_serial_redirect.py /dev/ttyUSB1 115200
python tcp_serial_redirect.py /dev/ttyACM0 115200
python tcp_serial_redirect.py /dev/ttyACM1 115200
