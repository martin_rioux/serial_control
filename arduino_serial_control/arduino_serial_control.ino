// By Martin Rioux -- martin.rioux2@gmail.com
// To use as you wish with proper credit!

//////////////////////////////////////////////////////////////
//
//  WASD pour controller les mouvements. La vitesse correspond a motorSpeed
//  1 pour diminuer vitesse, 2 pour augmenter, 3 vitesse possibles
//  Vitesse controlee par PWM sur pin 6 (left) et 7 (right)
//  PIN 13, 12, 8 et 7 controle la direction des moteurs
//
//  IJKL pour controller le canon
//  7 et 8 pour augmenter et diminuer vitesse
//
//////////////////////////////////////////////////////////////

#define ENABLE_PS_CONTROLLER 0 // SET TO 1 TO ENABLE PS2 CONTROLLER

#include <Servo.h> // WARNING: This library disable analogWrite() on pin 9 and 10


//CONFIGURATIONS


// MOTORS
const int leftMotorForward = 5;
const int leftMotorBackward = 4;
const int leftMotorSpeedPin = 6;
const int rightMotorForward = 9;
const int rightMotorBackward = 8;
const int rightMotorSpeedPin = 7;
int W = 0;
int A = 0;
int S = 0;
int D = 0;
int motorSpeed = 3;
int motorSpeedDiv = 10;

// SERVOS
const int Servo1Pin = 1;
Servo servo1;
const int Servo2Pin = 2;
Servo servo2;
int I = 0;
int K = 0;
int J = 0;
int L = 0;
int servoSpeed = 3;
int servoSpeedDiv = 10;
float servo1Pos = 90; // Change this for the default start position of servo1
float servo2Pos = 90; // Change this for the default start position of servo2

// GENERAL
String serial_data = "";
boolean serial_complete = false;

void setup() {
  serial_data.reserve(200);
  Serial.begin(115200);

  pinMode(rightMotorForward, OUTPUT);
  pinMode(rightMotorBackward, OUTPUT);
  pinMode(rightMotorSpeedPin, OUTPUT);
  pinMode(leftMotorForward, OUTPUT);
  pinMode(leftMotorBackward, OUTPUT);
  pinMode(leftMotorSpeedPin, OUTPUT);

  servo1.attach(Servo1Pin);
  servo1.write(servo1Pos);
  servo2.attach(Servo2Pin);
  servo2.write(servo2Pos);

  setup_ps_controller(); // does nothing if ENABLE_PS_CONTROLLER is 0
}


void loop() {
  handle_gamepad(); // does nothing if ENABLE_PS_CONTROLLER is 0
  loopingHandler();
  testSerialData();
  delay(10);
}


// Interrupt function
// Nothing here should cause delay!
void serialEvent() {
  //  Serial.print("readSerial");
  while (!serial_complete && Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '\n') {
      serial_complete = true;
      // Serial.println(serial_data);
    } else {
      serial_data += inChar;
    }
  }
}

void testSerialData() {
  //  Serial.print("useSerialData");
  if (serial_complete) {
    processSerialData(serial_data);
    serial_data = "";
    serial_complete = false;
  }
}
