

void processSerialData(String data){
  // THESE COMMANDS ARE EXECUTED ONCE UPON RECEIVAL
  // THERE IS ONE FOR PRESSED, RELEASED AND UNSPECIFIED
  // CAN BE USED TO SET SATES OF A VAR

  // IMPORTANT: Anyting called here should not take long to execute!

  // This is the continuous transmission processing received from the control software
  if (data[0] == '%'){
    for (int i=1; i < data.length(); i++){
      if      (data[i] == 'W') W = 1;
      else if (data[i] == 'w') W = 0;
      else if (data[i] == 'A') A = 1;
      else if (data[i] == 'a') A = 0;
      else if (data[i] == 'S') S = 1;
      else if (data[i] == 's') S = 0;
      else if (data[i] == 'D') D = 1;
      else if (data[i] == 'd') D = 0;
    
      else if (data[i] == 'K') K = 1;
      else if (data[i] == 'k') K = 0;
      else if (data[i] == 'I') I = 1;
      else if (data[i] == 'i') I = 0;
      else if (data[i] == 'J') J = 1;
      else if (data[i] == 'j') J = 0;
      else if (data[i] == 'L') L = 1;
      else if (data[i] == 'l') L = 0;
    }
  }


  // These are one shot commands received from the control software

  // Motor speed - 1
  else if (data == "P1") {
     motorSpeed -= 1;
     motorSpeed < 0 ? motorSpeed = 0 : 0;
     Serial.print("Motor Speed: ");
     Serial.println(motorSpeed);
  }
  // Motor speed + 1
  else if (data == "P2") {
     motorSpeed += 1;
     motorSpeed > motorSpeedDiv ? motorSpeed = motorSpeedDiv : 0;
     Serial.print("Motor Speed: ");
     Serial.println(motorSpeed);
  }

  // Servo speed - 1
  else if (data == "P7") {
     servoSpeed -= 1;
     servoSpeed < 0 ? servoSpeed = 0 : 0;
     Serial.print("Servo Speed: ");
     Serial.println(servoSpeed);
  }

  // Servo speed + 1
  else if (data == "P8") {
     servoSpeed += 1;
     servoSpeed > servoSpeedDiv ? servoSpeed = servoSpeedDiv : 0;
     Serial.print("Servo Speed: ");
     Serial.println(servoSpeed);
  }


}
