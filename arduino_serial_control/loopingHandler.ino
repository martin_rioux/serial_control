
// This function is called every loop in the main loop function
void loopingHandler(){
  movementHandler();
  servoHandler();
}


// Control motor speed and movement
void movementHandler(){
  float speedConversion = 0;
  speedConversion = map(motorSpeed, 0, motorSpeedDiv,25,255);

  analogWrite(leftMotorSpeedPin, speedConversion);
  analogWrite(rightMotorSpeedPin, speedConversion);

  if (W == 1){
    if (D == 1){                          // FORWARD-RIGHT
      digitalWrite(leftMotorForward, HIGH);
      digitalWrite(rightMotorForward, LOW);
      digitalWrite(leftMotorBackward, LOW);
      digitalWrite(rightMotorBackward, LOW);
    } else if (A == 1){                  // FORWARD-LEFT
      digitalWrite(leftMotorForward, LOW);
      digitalWrite(rightMotorForward, HIGH);
      digitalWrite(leftMotorBackward, LOW);
      digitalWrite(rightMotorBackward, LOW);
    } else {                             // FULL FORWARD
      digitalWrite(leftMotorForward, HIGH);
      digitalWrite(rightMotorForward, HIGH);
      digitalWrite(leftMotorBackward, LOW);
      digitalWrite(rightMotorBackward, LOW);
    }
  } else if (S == 1){
    if (D == 1){                          //BACKWARD-RIGHT
      digitalWrite(leftMotorForward, LOW);
      digitalWrite(rightMotorForward, LOW);
      digitalWrite(leftMotorBackward, HIGH);
      digitalWrite(rightMotorBackward, LOW);
    } else if (A == 1){                   // BACKWARD LEFT
      digitalWrite(leftMotorForward, LOW);
      digitalWrite(rightMotorForward, LOW);
      digitalWrite(leftMotorBackward, LOW);
      digitalWrite(rightMotorBackward, HIGH);
    } else {                              // FULL BACKWARD
      digitalWrite(leftMotorForward, LOW);
      digitalWrite(rightMotorForward, LOW);
      digitalWrite(leftMotorBackward, HIGH);
      digitalWrite(rightMotorBackward, HIGH);
    }
  } else if (D == 1){                    //FULL-RIGHT
      digitalWrite(leftMotorForward, HIGH);
      digitalWrite(rightMotorForward, LOW);
      digitalWrite(leftMotorBackward, LOW);
      digitalWrite(rightMotorBackward, HIGH);
  } else if (A == 1){                    //FULL-LEFT
      digitalWrite(leftMotorForward, LOW);
      digitalWrite(rightMotorForward, HIGH);
      digitalWrite(leftMotorBackward, HIGH);
      digitalWrite(rightMotorBackward, LOW);
  } else {                              // STOP
      digitalWrite(leftMotorForward, LOW);
      digitalWrite(rightMotorForward, LOW);
      digitalWrite(leftMotorBackward, LOW);
      digitalWrite(rightMotorBackward, LOW);
  }
}


// Control servo speed and movement
void servoHandler(){
  float speedConversion = 0;
  speedConversion = map(servoSpeed, 0, servoSpeedDiv, 1, 100);
  speedConversion = speedConversion/1000;

  if (I == 1) servo1Pos -= speedConversion;
  if (K == 1) servo1Pos += speedConversion;
  if (servo1Pos < 0) servo1Pos = 0;
  if (servo1Pos > 180) servo1Pos = 180;

  if (J == 1) servo2Pos -= speedConversion;
  if (L == 1) servo2Pos += speedConversion;
  if (servo2Pos < 0) servo2Pos = 0;
  if (servo2Pos > 180) servo2Pos = 180;

  servo1.write(servo1Pos);
  servo2.write(servo2Pos);
}
