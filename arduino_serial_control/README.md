# Code Arduino de la machine 
Le code exemple permet le déplacement avec un pont en H et 2 moteurs, ainsi que le contrôle de 2 servo moteurs.

## Installation
Installer Arduino IDE
Si vous employez la library PS2X_lib, copier le dossier ./libraries/PS2X_lip dans le dossier libraries de d'arduino sur votre ordinateur (Situé dans Documents sur windows).

## Fonctionnement
serialEvent() recoit la communication série. Lorsqu'une communication est complète (réception de \n), elle est traitée dans processSerialData(). Il s'agit de la place approprié pour une action immédiate. 

Pour les actions continues, l'idéal est de mettre un flag en place (ex.: W = 1; D = 1;), qui sera employé depuis la boucle principale loopingHandler(). 

### Mannettes de PS2
Si vous voulez employer une mannette de PS2 (sans fil), changez
`#define ENABLE_PS_CONTROLLER 0`
pour
`#define ENABLE_PS_CONTROLLER 1`

Puis sans le fichier ps2_controller.ino, changez les numéro de pins de la configuration tel que:
```
// setup pins and settings:  GamePad(clock, command, attention, data, Pressures?, Rumble?) check for error
error = ps2x.config_gamepad(13,11,10,12, true, true);
```


## ATTENTION
Les commandes delay(n), c'est le mal, ne jamais employer ça si vous voulez un code qui travail en 'parallèle'. Employer des timers logiciels à la place. Ex.:

```c++
void start_timer(){
	...
	awesome_timer = millis(); // awesome_timer type must be 'unsigned long', since an int would overflow too quickly
	...
}

void check_timer(){
	...
	if (millis() - awesome_timer > wait_time){
		do_awesome_stuff();
	}
	...
}

```