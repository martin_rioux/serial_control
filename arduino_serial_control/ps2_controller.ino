

#if (ENABLE_PS_CONTROLLER)
int debug_ps_controller = 0;
#include <PS2X_lib.h> 
PS2X ps2x;
int error = 0;
int vibrate = 0;
byte PS2_type = 0;
byte PS2_vibrate = 0;

// PS2 Controller features
void setup_ps_controller()
{
  // setup pins and settings:  GamePad(clock, command, attention, data, Pressures?, Rumble?) check for error
  error = ps2x.config_gamepad(13,11,10,12, true, true);

  if(error == 0 )
  {
    Serial.println("Found Controller, configured successful");
  }
  else if(error == 1)
    Serial.println("No controller found, check wiring, see readme.txt to enable debug. visit www.billporter.info for troubleshooting tips");
  else if(error == 2)
    Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");
  else if(error == 3)
    Serial.println("Controller refusing to enter Pressures mode, may not support it. ");

  // Serial.print(ps2x.Analog(1), HEX);
  PS2_type = ps2x.readType();
  switch(PS2_type)
  {
    case 0:
      Serial.println("Unknown Controller type (lynxmotion)");
      break;
    case 1:
      Serial.println("DualShock Controller Found");
      break;
    case 2:
      Serial.println("GuitarHero Controller Found");
      break;
  }
}

void handle_gamepad()
{
  ps2x.read_gamepad();
  if (debug_ps_controller){
    Serial.print("PSB_PAD_UP: ");
    Serial.print(ps2x.Button(PSB_PAD_UP));
    Serial.print(", PSB_PAD_DOWN: ");
    Serial.print(ps2x.Button(PSB_PAD_DOWN));
    Serial.print(", PSB_PAD_LEFT: ");
    Serial.print(ps2x.Button(PSB_PAD_LEFT));
    Serial.print(", PSB_PAD_RIGHT: ");
    Serial.print(ps2x.Button(PSB_PAD_RIGHT));
    Serial.print(", PSB_GREEN: ");
    Serial.print(ps2x.Button(PSB_GREEN));
    Serial.print(", PSB_BLUE: ");
    Serial.print(ps2x.Button(PSB_BLUE));
    Serial.print(", motorSpeed: ");
    Serial.print(motorSpeed);
    Serial.print(", liftMotor Speed: ");
    Serial.println(servoSpeed);
  }

  ps2x.Button(PSB_PAD_UP) ? W=1 : W=0;
  ps2x.Button(PSB_PAD_LEFT) ? A=1 : A=0;
  ps2x.Button(PSB_PAD_DOWN) ? S=1 : S=0;
  ps2x.Button(PSB_PAD_RIGHT) ? D=1 : D=0;

  if (ps2x.ButtonPressed(PSB_R2)){
    motorSpeed += 1;
    motorSpeed > motorSpeedDiv ? motorSpeed = motorSpeedDiv : 0;
  }

  if (ps2x.ButtonPressed(PSB_L2)){
    motorSpeed -= 1;
    motorSpeed < 0 ? motorSpeed = 0 : 0;
  }


  ps2x.Button(PSB_GREEN) ? I=1 : I=0;
  ps2x.Button(PSB_BLUE) ? K=1 : K=0;
  ps2x.Button(PSB_PINK) ? J=1 : J=0;
  ps2x.Button(PSB_RED) ? L=1 : L=0;

  if (ps2x.ButtonPressed(PSB_R1)){
    servoSpeed += 1;
    servoSpeed > servoSpeedDiv ? servoSpeed = servoSpeedDiv : 0;
  }

  if (ps2x.ButtonPressed(PSB_L1)){
    servoSpeed -= 1;
    servoSpeed < 0 ? servoSpeed = 0 : 0;
  }
}


#else

void setup_ps_controller()
{
  return;
}
void handle_gamepad()
{
  return;
}

#endif
